﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProteinSeqSimilarity
{
    /// <summary>
    /// Factory selector of strategies.
    /// </summary>
    class AlgoFactory
    {
        private static IAlignAlgo createEdit()
        {
            //nepotrebuje settings
            return new EditDistance();
        }

        private static IAlignAlgo createHamming()
        {
            //nepotrebuje settings
            return new HammingDistance();
        }

        private static IAlignAlgo createOWED()
        {
            string[] settings = Environment.GetCommandLineArgs();
            int gap = int.Parse(settings[2]);
            int subs = int.Parse(settings[3]);
            int eq = int.Parse(settings[4]);
            return new OWED(gap, subs, eq);
        }

        private static IAlignAlgo createAWED()
        {
            string[] settings = Environment.GetCommandLineArgs();
            int gap = int.Parse(settings[2]);
            string tabpath = settings[3];
            ScoringTab tab = ScoringTab.Load(tabpath);
            return new AWED(gap, tab);
        }

        public static IAlignAlgo Get(string method)
        {
            switch (method.ToLower())
            {
                case "edit":
                    return createEdit();
                case "hamming":
                    return createHamming();
                case "owed":
                    return createOWED();
                case "awed":
                    return createAWED();
                default:
                    throw new NotImplementedException();
            }
        }
    }

    /// <summary>
    /// Context class for getting similarity score.
    /// </summary>
    class Similarity
    {
        public double GetSimilarity(ISequenceRepresentation sq1, ISequenceRepresentation sq2, string method)
        {
            IAlignAlgo algorithm = AlgoFactory.Get(method); //choosing a strategy
            int score = algorithm.GetAlignmentScore(sq1, sq2); //run algorithm
            return score;
        }
    }
}
