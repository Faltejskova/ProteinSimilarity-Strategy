﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProteinSeqSimilarity
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length <= 1)
            {
                writeInstructions();
                Console.ReadLine();
                Environment.Exit(1);
            }
            string fastaname = args[0];
            string method = args[1];
            List<FastaModel> fasta = FastaParser.Parse(fastaname);
            if (fasta.Count < 2)
            {
                Console.WriteLine("Not enough sequences.");
                Environment.Exit(1);
            }
            FastaModel fasta1 = fasta[0];
            FastaModel fasta2 = fasta[1];
            var sim = new Similarity(); //context class for strategy pattern
            try
            {
                double score = sim.GetSimilarity(fasta1, fasta2, method);
                Console.WriteLine(score);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(1);
            }
        }

        static void writeInstructions()
        {
            Console.WriteLine(
                "Prvním argumentem je název FASTA souboru s alespoň 2 sekvencemi.\nDruhým argumentem je název algoritmu k použití: \nedit - edit distance = minimální počet změn (substituce, delece) mezi sekvencemi\nhamming - hammingova vzdálenost = počet rozdílných pozic (sekvence musí mít stejnou délku)\nowed - edit distance vážená podle operací, vyžaduje parametrem skóre mezery (delece), skóre substituce a skóre stejných residuí pod sebou v alignmentu\nawed - edit distance vážená podle abecedy, vyžaduje parametrem skóre mezery a název souboru se skórovací tabulkou.\nDalší parametry následují podle použitého algoritmu."
                );
            Console.WriteLine();
            Console.WriteLine("Stiskněte ENTER k ukončení programu.");
        }
    }
}
