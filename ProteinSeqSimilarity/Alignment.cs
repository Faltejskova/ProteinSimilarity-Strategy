﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ProteinSeqSimilarity
{
    /// <summary>
    /// Scoring table representation defining substitution cost of 2 characters.
    /// </summary>
    class ScoringTab
    {
        private Dictionary<char, int> aaToIndex;

        private Dictionary<string, int> tab;
        public ScoringTab(Dictionary<string, int> tab, Dictionary<char, int> aaToIndex)
        {
            this.aaToIndex = aaToIndex;
            this.tab = tab;
        }

        /// <summary>
        /// Loads a scoring table from a text file.
        /// </summary>
        /// <param name="path">Name of source file.</param>
        /// <returns></returns>
        public static ScoringTab Load(string path) //loads from file, file is provided.
        {
            var lines = File.ReadLines(path);
            var aminoacids = lines.First().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var aatoindex = new Dictionary<char, int>();
            for (int k = 0; k < aminoacids.Length; k++)
            {
                aatoindex.Add(aminoacids[k][0], k);
            }

            var tab = new Dictionary<string, int>();
            int i = 1;
            foreach (var line in lines.Skip(1))
            {
                string[] splits = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                for (int j = 1; j <= i; j++)
                {
                    string code = $"{aminoacids[j - 1]}{splits[0]}";
                    tab.Add(code, int.Parse(splits[j]));
                }
                i++;
            }
            return new ScoringTab(tab, aatoindex);
        }

        public int Cost(char a, char b)
        {
            if (aaToIndex[a] > aaToIndex[b])
            {
                char help = a;
                a = b;
                b = help;
            }
            string code = $"{a}{b}";
            return tab[code];
        }
    }

    /// <summary>
    /// Interface grouping algorithms for getting protein sequence alignment score - strategies.
    /// </summary>
    interface IAlignAlgo
    {
        int GetAlignmentScore(ISequenceRepresentation sq1, ISequenceRepresentation sq2);
    }

    /// <summary>
    /// Alphabet weighted edit distance, dependent on a scoring table. Concrete strategy to compute protein sequence alignment score.
    /// </summary>
    class AWED : IAlignAlgo
    {
        int gapPenalty = 0;
        ScoringTab scoreTab;
        public AWED(int gapPenalty, ScoringTab scoreTab)
        {
            this.gapPenalty = gapPenalty;
            this.scoreTab = scoreTab;
        }

        public int GetAlignmentScore(ISequenceRepresentation sq1, ISequenceRepresentation sq2)
        {
            int[,] tab = DynamicBase.FillTab(gapPenalty, sq1.Sequence, sq2.Sequence, (a, b) => scoreTab.Cost(a, b));
            return tab[sq1.Length, sq2.Length];
        }
    }

    /// <summary>
    /// Operation weighted edit distance, requires penalties for deletion, substitution, match. Concrete strategy to compute protein sequence alignment score.
    /// </summary>
    class OWED : IAlignAlgo
    {
        int gapPenalty = 0;
        int subsPenalty = 0;
        int eqPenalty = 0;

        public OWED(int gapPenalty, int subsPenalty, int eqPenalty)
        {
            this.gapPenalty = gapPenalty;
            this.subsPenalty = subsPenalty;
            this.eqPenalty = eqPenalty;
        }

        private int cost(char a, char b)
        {
            if (a == b) return eqPenalty;
            return subsPenalty;
        }

        public int GetAlignmentScore(ISequenceRepresentation sq1, ISequenceRepresentation sq2)
        {
            int[,] tab = DynamicBase.FillTab(gapPenalty, sq1.Sequence, sq2.Sequence, (a,b) => cost(a, b));
            return tab[sq1.Length, sq2.Length];
        }
    }

    /// <summary>
    /// Basic alignment method - dynamic programming.
    /// </summary>
    class DynamicBase
    {
        private static int min(int a, int b, int c)
        {
            if (a < b && a < c) return a;
            if (b < c) return b;
            return c;
        }

        public static int[,] FillTab(int gapPenalty, string sq1, string sq2, Func<char, char, int> cost)
        {
            int[,] tab = new int[sq1.Length + 1, sq2.Length + 1];
            for (int i = 1; i <= sq1.Length; i++) tab[i, 0] = i * gapPenalty; //gap penalty
            for (int j = 1; j <= sq2.Length; j++) tab[0, j] = j * gapPenalty; //gap penalty

            for (int i = 1; i <= sq1.Length; i++)
                for (int j = 1; j <= sq2.Length; j++)
                {
                    
                    tab[i, j] = min(gapPenalty + tab[i - 1, j],
                               gapPenalty + tab[i, j - 1],
                            cost(sq1[i - 1], sq2[j - 1]) + tab[i - 1, j - 1]);
                    int tabs = tab[i, j];
                }
            return tab;
        }
    }

    /// <summary>
    /// Concrete strategy to compute protein sequence alignment score, based on minimal count of mutations from seq1 to seq2.
    /// </summary>
    class EditDistance : IAlignAlgo
    {
        private int cost(char a, char b)
        {
            if (a == b) return 0;
            return 1;
        }
        /// <summary>
        /// Counts edit distance of two sequnces using dynamic programming.
        /// </summary>
        /// <returns>edit distance of sequences</returns>
        public int GetAlignmentScore(ISequenceRepresentation sq1, ISequenceRepresentation sq2)
        {
            int[,] tab = DynamicBase.FillTab(1, sq1.Sequence, sq2.Sequence, (a, b) => cost(a, b));
            return tab[sq1.Length, sq2.Length];
        }
    }

    /// <summary>
    /// Basic measurement of protein sequence similarity - ammount of different positions. Can probe only sequences of the same lenght. Concrete strategy to compute protein sequence alignment score.
    /// </summary>
    class HammingDistance : IAlignAlgo
    {
        public int GetAlignmentScore(ISequenceRepresentation sq1, ISequenceRepresentation sq2)
        {
            if (sq1.Length != sq2.Length) throw new ArgumentException("Input sequences are required to have the same lenght.");
            int result = 0;

            var sq1rep = sq1.Sequence;
            var sq2rep = sq2.Sequence;

            for (int i = 0; i < sq1.Length; i++)
            {
                if (sq1rep[i] != sq2rep[i])
                    result++;
            }

            return result;
        }
    }
}
